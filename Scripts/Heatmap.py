# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:09:34 2019

@author: Pablo_A
"""
import seaborn as sns
import pandas as pd
from matplotlib import pyplot as plt

#Lectura de la tabla
df = pd.read_csv("C:\\Users\\Manu\\Desktop\\TranscriptomicAnalisis\\Datos\\Raw_correg_merged.txt", sep="\t")

df.rename({'Unnamed: 0': 'Transcript Code'}, axis=1, inplace=True)

#Definimos el indice como el codigo de los transcriptos
df = df.set_index('Transcript Code')

"""
###Heatmap de todos los transcriptos
#Reemplazamos los valores NaN como cero, ya que el heatmap no admite NaN
sin_na = df.loc[:, ["logFC_1h","logFC_24h"]].fillna(0)

# Graficamos el heatmap
sns.clustermap(sin_na)
plt.title("Fold Change a 1 y 24 horas")
plt.show()
"""


### Heatmap con los transcriptos diferencialmente expresados
#filtramos los transcriptos diferencialmente expresados
deg_sin_na = df.loc[(df["fdr.gen_1h"] < 0.05)|(df["fdr.gen_24h"] < 0.05), ["logFC_1h","logFC_24h"]].fillna(0)

# Graficamos el heatmap
sns.clustermap(deg_sin_na)
plt.title("DEGs a 1 y 24 horas")


plt.savefig('Heatmap_DEG1hvs24hs.png')

"""
###Heatmap clustereado por metodo ward
#definimos los transcriptos diferencialmente expresados
deg_sin_na = df.loc[(df["fdr.gen_1h"] < 0.05)|(df["fdr.gen_24h"] < 0.05), ["logFC_1h","logFC_24h"]].fillna(0)

# Graficamos el heatmap
sns.clustermap(deg_sin_na, metric="euclidean", standard_scale=1, method="ward")
plt.title("DEGs a 1 y 24 horas")
plt.show()
"""



