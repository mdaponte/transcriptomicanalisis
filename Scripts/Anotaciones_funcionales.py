# -*- coding: utf-8 -*-
"""
Created on Sab Oct 26 13:07:34 2019

@author: Pablo_A
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import collections
import numpy as np

#Lectura de la tabla filtrada y anotada
anotada = pd.read_csv("./Datos/filtrada_anotada.txt", sep= "\t")

#Filtramos los datos para la categoria Molecular Functions
molecularF = anotada.loc[anotada.loc[:, "Aspect"] == "F"]

#Contamos la cantidad de genes en los que aparece cada termino GO
conteo = collections.Counter(list(molecularF.loc[:,"GO_term"]))
#Seleccionamos los 10 elementos mas frecuentes
conteo.most_common(10)

#Preparamos las variables para el grafico
n_groups = len(conteo.most_common(10))
valores = [x[1] for x in conteo.most_common(10)]
legendas = [x[0] for x in conteo.most_common(10)]

#Generamos el grafico
fig,ax = plt.subplots(1,2)
#Ocultamos un grafico para que las anotaciones tengan mas lugar en la imagen y no se corten
ax[0].spines["bottom"].set_color('none')
ax[0].spines["top"].set_color('none')
ax[0].spines["left"].set_color('none')
ax[0].spines["right"].set_color('none')
ax[0].tick_params(axis='x', colors='none')
ax[0].tick_params(axis='y', colors='none')


index = np.arange(n_groups)
bar_width = 0.25
opacity = 0.4
rects1 = plt.barh(index, valores, bar_width,
                 alpha=opacity, color='b', label='Ocurrencia')
plt.ylabel('Funcion Molecular', fontsize=15, fontname="sans-serif", fontweight="bold")
plt.xlabel("Numero de genes", fontsize=15, fontname="sans-serif", fontweight="bold")
plt.title('Anotaciones GO mas frecuentes', fontsize=15, fontname="sans-serif", fontweight="bold")
plt.yticks(index + bar_width, legendas, fontsize=12, fontname="sans-serif")
plt.tight_layout(pad = 0.7)
#Guardar el grafico
plt.savefig('molecularf.png', format='png', bbox_inches='tight', dpi=300)
plt.close()
