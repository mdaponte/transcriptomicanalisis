# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:09:34 2019

@author: Manu
"""
import pandas as pd

Raw_data_1h = pd.read_csv("C:\\Users\\Manu\\Desktop\\Raw_data_WT_1h.txt",sep="\t")

Raw_data_24h = pd.read_csv("C:\\Users\\Manu\\Desktop\\Raw_data_WT_24h.txt",sep="\t")



Raw_data_24h["fdr.gen"] = Raw_data_24h["fdr.gen"].str.replace(",", ".").astype("float64")

Raw_data_24h["logFC"] = Raw_data_24h["logFC"].str.replace(",", ".").astype("float64")



Raw_data_1h["fdr.gen"] = Raw_data_1h["fdr.gen"].str.replace(",", ".").astype("float64")

Raw_data_1h["logFC"] = Raw_data_1h["logFC"].str.replace(",", ".").astype("float64")



DEG_filter_1h = Raw_data_1h[(abs(Raw_data_1h.logFC) > 0.58) & (Raw_data_1h["fdr.gen"] < 0.05)]

DEG_filter_24h = Raw_data_24h[(abs(Raw_data_24h.logFC) > 0.58) & (Raw_data_24h["fdr.gen"] < 0.05)]


DEG_filter_1h.to_csv("C:\\Users\\Manu\\Desktop\\DEG_filter_pandas_1h.txt",sep="\t",index=False)

DEG_filter_24h.to_csv("C:\\Users\\Manu\\Desktop\\DEG_filter_pandas_24h.txt",sep="\t",index=False)
