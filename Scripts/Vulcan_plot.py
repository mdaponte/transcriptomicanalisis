# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:09:34 2019

@author: Pablo_A
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Lectura de la tabla
d = pd.read_csv("Raw_correg_merged.txt", sep="\t")

### a 1 hora
#Defenir genes diferencialmente expresados y asociar color
d.loc[(d["logFC_1h"] >= 1) & (d["fdr.gen_1h"] < 0.05), 'color_1h'] = "green"  # sobreexpresados
d.loc[(d["logFC_1h"] <= -1) & (d["fdr.gen_1h"] < 0.05), 'color_1h'] = "red"  # subexpreseados
d['color_1h'].fillna('grey', inplace=True)  # intermedio

#Ajustar el p valor para el grafico
d['logpv_1h'] = -(np.log10(d["fdr.gen_1h"]))
    
### a 24 horas
#Defenir genes diferencialmente expresados y asociar color
d.loc[(d["logFC_24h"] >= 1) & (d["fdr.gen_24h"] < 0.05), 'color_24h'] = "green"  # sobreexpresados
d.loc[(d["logFC_24h"] <= -1) & (d["fdr.gen_24h"] < 0.05), 'color_24h'] = "red"  # subexpreseados
d['color_24h'].fillna('grey', inplace=True)  # intermedio

#Ajustar el p valor para el grafico
d['logpv_24h'] = -(np.log10(d["fdr.gen_24h"]))

# Volcano plot a 1 hora
plt.subplot(1,2,1)    
plt.scatter(d["logFC_1h"], d['logpv_1h'], c=d['color_1h'])
plt.title("a 1 hora")
plt.xlabel('log2 Fold Change', fontsize=15, fontname="sans-serif", fontweight="bold")    
plt.ylabel('-log10(P-value)', fontsize=15, fontname="sans-serif", fontweight="bold")
plt.xticks(fontsize=12, fontname="sans-serif")
plt.yticks(fontsize=12, fontname="sans-serif")
    
# Volcano plot a 24 horas
plt.subplot(1,2,2)    
plt.scatter(d["logFC_24h"], d['logpv_24h'], c=d['color_24h'])
plt.title("a 24 horas")
plt.xlabel('log2 Fold Change', fontsize=15, fontname="sans-serif", fontweight="bold")    
plt.ylabel('-log10(P-value)', fontsize=15, fontname="sans-serif", fontweight="bold")
plt.xticks(fontsize=12, fontname="sans-serif")
plt.yticks(fontsize=12, fontname="sans-serif")
plt.savefig('volcano.png', format='png', bbox_inches='tight', dpi=300)
plt.close()

