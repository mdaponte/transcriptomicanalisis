# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 14:09:34 2019

@author: Pablo_A
"""
import pandas as pd

#Lectura de los archivos
Raw_data_1h = pd.read_csv("Raw_data_WT_1h.txt",sep="\t")

Raw_data_24h = pd.read_csv("Raw_data_WT_24h.txt",sep="\t")

#Correccion de la tabla, cambiamos la coma por el punto
#para que Python reconosca la notacion cientifica
Raw_data_1h["fdr.gen_1h"] = Raw_data_1h["fdr.gen"].str.replace(",", ".").astype("float64")

Raw_data_1h["logFC_1h"] = Raw_data_1h["logFC"].str.replace(",", ".").astype("float64")

Raw_data_24h["fdr.gen_24h"] = Raw_data_24h["fdr.gen"].str.replace(",", ".").astype("float64")

Raw_data_24h["logFC_24h"] = Raw_data_24h["logFC"].str.replace(",", ".").astype("float64")

#Union de las tablass 1h y 24h
Merged_table_1vs24 = pd.merge(
left=Raw_data_1h.drop(["logFC","fdr.gen"], axis=1), 
right=Raw_data_24h.drop(["logFC","fdr.gen"], axis=1), 
left_on=["Unnamed: 0",'commName', 'coordGene', 'start', 'end', 'width','effWidth'], 
right_on=["Unnamed: 0",'commName', 'coordGene', 'start', 'end', 'width','effWidth'],
how="outer")

#Guardado de la tabla
Merged_table_1vs24.to_csv("Raw_correg_merged.txt",sep="\t",index=False)

